//
//  KatzeTests.swift
//  KatzeTests
//
//  Created by Carlos Corrêa on 02/06/17.
//  Copyright © 2017 ccorrea. All rights reserved.
//

import XCTest
@testable import Katze

class KatzeTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_catAge() {
        let catAgeChecker = CatAgeChecker()
        let inputAge = 5
        let expectedAge = 35
        let catAge = catAgeChecker.check(age: inputAge)
        
        XCTAssertTrue(catAge == expectedAge, "Cat age is wrong")
    }
    
    func test_llamaAge() {
        let llamaAgeChecker = StubLlamaAgeChecker()
        let inputAge = 1
        let expectedAge = 15
        let llamaAge = llamaAgeChecker.check(age: inputAge)
        
        XCTAssertTrue(llamaAge == expectedAge, "Llama age is wrong")
    }
}
