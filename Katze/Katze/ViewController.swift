//
//  ViewController.swift
//  Katze
//
//  Created by Carlos Corrêa on 02/06/17.
//  Copyright © 2017 ccorrea. All rights reserved.
//

import UIKit

struct KeyboardWillShowPayload {
    let endFrame: CGRect
    init(userInfo: [AnyHashable: Any]) {
        endFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect ?? CGRect.zero
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    var initialBottomLayoutConstant: CGFloat?
    let catAgeChecker = CatAgeChecker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerKeyboardNotifications()
    }
    
    @IBAction func checkButtonTouched(_ sender: Any) {
        let age = Int(ageTextField.text ?? "0") ?? 0
        revealCat(age: age)
    }
    
    private func revealCat(age: Int) {
        resetUI()
        
        let catAge = catAgeChecker.check(age: age)
        let message = "\(catAge) anos"
        showAlertWith(title: "A idade do seu gato é", message: message)
    }
    
    private func showAlertWith(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        alert.addAction(okAction)
        present(alert, animated: true)
    }
    
    private func resetUI() {
        ageTextField.text = ""
        ageTextField.resignFirstResponder()
    }
    
    // MARK: - UIKeyboard notifications
    
    private func registerKeyboardNotifications() {
        initialBottomLayoutConstant = bottomLayoutConstraint.constant
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: nil) { [weak self] note in
            let payload = KeyboardWillShowPayload(userInfo: note.userInfo ?? [:])
            self?.bottomLayoutConstraint.constant = payload.endFrame.height
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: nil) { [weak self] _ in
            self?.bottomLayoutConstraint.constant = self?.initialBottomLayoutConstant ?? 0
        }
    }
    
    private func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    deinit {
        unregisterKeyboardNotifications()
    }
}
