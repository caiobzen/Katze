//
//  CatAgeChecker.swift
//  Katze
//
//  Created by Carlos Corrêa on 02/06/17.
//  Copyright © 2017 ccorrea. All rights reserved.
//

import Foundation

class CatAgeChecker: AnimalAgeChecker {

    func check(age: Int) -> Int {
        return age * 7
    }
}
