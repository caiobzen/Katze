//
//  XCTestCase+Helper.swift
//  Katze
//
//  Created by Giovane Possebon on 02/06/17.
//  Copyright © 2017 ccorrea. All rights reserved.
//

import XCTest

extension XCTestCase {
    
    func waitForSplashScreenAnimation() {
        sleep(3)
    }
    
    func waitForElementToAppear(element: XCUIElement, timeout: TimeInterval = 5,  file: String = #file, line: UInt = #line) {
        let existsPredicate = NSPredicate(format: "exists == true")
        
        expectation(for: existsPredicate, evaluatedWith: element, handler: nil)
        
        waitForExpectations(timeout: timeout) { (error) in
            if (error != nil) {
                let message = "Failed to find \(element) after \(timeout) seconds."
                self.recordFailure(withDescription: message, inFile: file, atLine: line, expected: true)
            }
        }
    }
    
}

