//
//  KatzeUITests.swift
//  KatzeUITests
//
//  Created by Giovane Possebon on 02/06/17.
//  Copyright © 2017 ccorrea. All rights reserved.
//

import XCTest

class KatzeHomeScreen: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        let app = XCUIApplication()
        
        continueAfterFailure = false
        app.launch()
        
        waitForSplashScreenAnimation()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_CatAgeCalcSimulation() {
        let app = XCUIApplication()
        
        let ageTextField = app.textFields["TextFieldCatAge"]
        waitForElementToAppear(element: ageTextField)
        XCTAssert(ageTextField.exists)
        
        ageTextField.tap()
        ageTextField.typeText("7")
        
        let ageCalcButton = app.buttons["ButtonCatAgeCalc"]
        waitForElementToAppear(element: ageCalcButton)
        XCTAssert(ageCalcButton.exists)
        
        ageCalcButton.tap()
        
        let alertView = app.alerts["A idade do seu gato é"]
        waitForElementToAppear(element: alertView)
        XCTAssert(alertView.exists)
        XCTAssert(alertView.staticTexts["49 anos"].exists)
        
        let alertViewOkButton = alertView.buttons["OK"]
        waitForElementToAppear(element: alertViewOkButton)
        XCTAssert(alertViewOkButton.exists)
        
        alertViewOkButton.tap()
        
        XCTAssert(alertView.exists == false)
    }
    
}
